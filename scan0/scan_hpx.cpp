#include <hpx/hpx_init.hpp>
#include <hpx/include/async.hpp>
#include <hpx/include/util.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <boost/program_options.hpp>

/**
   Cannot use std::partial_sum directly in the code since the vector in on ints and not iterators.
   Could we make it on iterators?
 */
template <typename Iter>
int s_partial_sum(Iter begin, Iter end) {
    (*(std::partial_sum(begin, end, begin)-1));
    return 0;
}

/**
   Could not use std::for_each directly because the operator type is not known when instantiating async,
   unless we write a functor instead of a lambda, but at this point it's just the same as duing this,
   actually, this is less verbose, probably
 */
template <typename Iter>
int update(Iter begin, Iter end, int the_value) {
    std::for_each(begin, end, [the_value](int& v) {v+= the_value;});
    return 0;
}

template <typename Iter>
int parallel_partial_sum(int chunks, Iter begin, Iter end) {


    auto length = std::distance(begin, end);
    auto chunk_size = length/chunks;
    if (chunk_size < 1) {
        chunks = 1;
        chunk_size = length;
    }

    hpx::naming::id_type const locality_id = hpx::find_here();

    std::vector<hpx::future<int>> partials;
    std::vector<int>              __partials(chunks);
    
    auto chunk_begin = begin;
    for (int i = 0; i < chunks-1; ++i) {
        partials.push_back(hpx::async(&s_partial_sum<Iter>/*, locality_id*/, chunk_begin, chunk_begin + chunk_size));
        chunk_begin += chunk_size;
    }
    partials.push_back(hpx::async(&s_partial_sum<Iter>/*, locality_id*/, chunk_begin, end));

    __partials[0] = partials[0].get();
    for (int i = 1; i < chunks; ++i) {
        __partials[i] = __partials[i-1] + partials[i].get();
    }

#ifdef OUT
    std::for_each(__partials.begin(), __partials.end(), [](int const& v) { std::cout << v << ", ";});
    std::cout << std::endl;
#endif
    
#ifdef OUT
    std::for_each(begin, end, [](int const& v) { std::cout << v << ", ";});
    std::cout << std::endl;
#endif

    partials.clear();
    chunk_begin = begin + chunk_size;
    for (int i = 1; i < chunks-1; ++i) {
        partials.push_back(hpx::async(&update<Iter>/*, locality_id*/, chunk_begin, chunk_begin + chunk_size, __partials[i-1]));
        chunk_begin += chunk_size;
    }
    partials.push_back(hpx::async(&update<Iter>/*, locality_id*/, chunk_begin, end,  __partials[chunks-2]));

    
    return 0;

}

template <typename T>
void parallel(std::vector<int> & data, T chunks = 8u) {

    

    auto start = std::chrono::high_resolution_clock::now();

    parallel_partial_sum(chunks, data.begin(), data.end());

    std::chrono::duration<double> diff =  std::chrono::high_resolution_clock::now()-start;
    std::cout << "Time parallel in " << diff.count() << std::endl;

}


void sequential(std::vector<int> & data) {

    auto start = std::chrono::high_resolution_clock::now();

    std::partial_sum(data.begin(), data.end(), data.begin());

    std::chrono::duration<double> diff =  std::chrono::high_resolution_clock::now()-start;
    std::cout << "Time Sequential in " << diff.count() << std::endl;
}


int hpx_main(boost::program_options::variables_map& vm) {

    int problem_size = vm["n"].as<int>();
    std::cout << "Input size = " << problem_size << std::endl;

    std::vector<int> input_data_p(problem_size);

    int i = 1;
    std::for_each(input_data_p.begin(), input_data_p.end(), [&i](int & v) { v=i++; });

#ifdef OUT
    std::for_each(input_data_p.begin(), input_data_p.end(), [](int const& v) { std::cout << v << ", ";});
    std::cout << std::endl;
#endif

    auto start = std::chrono::high_resolution_clock::now();
    parallel(input_data_p, vm["c"].as<int>());
    std::chrono::duration<double> diff =  std::chrono::high_resolution_clock::now()-start;
    std::cout << "Time Parallel out " << diff.count() << std::endl;


    std::vector<int> input_data_s(problem_size);

    i = 1;
    std::for_each(input_data_s.begin(), input_data_s.end(), [&i](int & v) { v=i++; });

#ifdef OUT
    std::for_each(input_data_s.begin(), input_data_s.end(), [](int const& v) { std::cout << v << ", ";});
    std::cout << std::endl;
#endif

    start = std::chrono::high_resolution_clock::now();
    sequential(input_data_s);
    diff =  std::chrono::high_resolution_clock::now()-start;
    std::cout << "Time Sequential out " << diff.count() << std::endl;



    std::cout << "PASSED " << std::boolalpha
              << std::equal(input_data_p.begin(), input_data_p.end(), input_data_s.begin()) << std::endl;
    std::cout << "PASSED " << std::boolalpha
              << std::equal(input_data_s.begin(), input_data_s.end(), input_data_p.begin()) << std::endl;
    
    return hpx::finalize();
}

int main(int argc, char** argv) {

    namespace po = boost::program_options;

    po::options_description desc_commandline;
    desc_commandline.add_options()
        ("n", po::value<int>()->default_value(1024), "Input size")
        ("c", po::value<int>()->default_value(8), "Number of chunks (threads)");

    return hpx::init(desc_commandline, argc, argv);
}


